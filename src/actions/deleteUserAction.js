import { DELETE_USER_SUCCESS, DELETE_USER_ERROR } from "../constants/deleteUser";
import axios from 'axios';
export const deleteUserAction = (username) => async dispatch => {

   
    try {
        var url = `https://chilling-crypt-20162.herokuapp.com/api/v1/users/username/${username}`;
        var token = localStorage.getItem("token");
        var requestOptions = {
            headers: {
                'Authorization': 'Token ' + token,
            }
        }
        const res = await axios.delete(url, requestOptions);
        return dispatch({
            type: DELETE_USER_SUCCESS,
            data: res.data,
        });
    } catch (err) {
        await dispatch({
          type: DELETE_USER_ERROR,
          error: err
        });
    }
};


