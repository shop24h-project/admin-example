import {
    GET_ALL_USER,
    } from "../constants/getUser.js";
    import axios from 'axios';
    export const getAllUser = () => async dispatch => {

      var url = `https://chilling-crypt-20162.herokuapp.com/api/v1/users`;
      try {
        const res = await axios.get(url);
            return dispatch({
                type: GET_ALL_USER,
                data: res.data,
            });
      } catch (err) {
          console.log(err);
      }
    }
