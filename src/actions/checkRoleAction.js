import {
    CHECK_ROLE,
} from "../constants/checkRole.js";
import axios from 'axios';
export const checkRoleAction = () => async dispatch => {

    var url = `https://chilling-crypt-20162.herokuapp.com/api/v1/hello-admin`;
    var token = localStorage.getItem("token");
    var requestOptions = {
        headers: {
            'Authorization': 'Token ' + token,
        }
    }
    try {
        const res = await axios.get(url, requestOptions);
        return dispatch({
            type: CHECK_ROLE,
            data: res.data,
        });
    } catch (err) {
        console.log(err);
    }
}
