import React, { useEffect, useState } from "react";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { useForm } from "react-hook-form";

function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const theme = createTheme();

export default function SignIn() {
  const navigate = useNavigate();
  const [errorUserName, setErrorUserName] = useState(false);
  const [errorPassword, setErrorPassword] = useState(false);
  const [error, setError] = useState(false);
  const [errorText, setErrorText] = useState(false);
  const [errorLoginText, setErrorLoginText] = useState("");

  const onSubmit = async ({ userName, password }) => {
    try {
      var newData = {
        username: userName,
        password: password,
      }
        const res = await axios.post('https://chilling-crypt-20162.herokuapp.com/api/v1/login',newData);
        console.log(res.data);
        localStorage.setItem("token", res.data);
        navigate("/");
      
    } catch (err) {
      setError(true);
      setErrorLoginText("Tài khoản hoặc mật khẩu không chính xác")
    }
  }
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              error={!!errors?.userName}
              helperText={errors?.userName ? errors.userName.message : null}
              id="name"
              label="User Name"
              name="userName"
              autoComplete="User Name"
              autoFocus
              {...register("userName", {
                required: "Bạn chưa nhập User Name",
              })}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              error={!!errors?.password}
              helperText={errors?.password ? errors.password.message : null}
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              {...register("password", {
                required: "Bạn chưa nhập Password",
              })}
            />
              <Typography component="h1" variant="h6" style={{ color:"red", textAlign: "center", fontSize: "12px"}}>
                {  errorLoginText  }
          </Typography>
            {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 1, mb: 2 }}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 8, mb: 4 }} />
      </Container>
    </ThemeProvider>
  );
}
