
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import { Button, TableContainer, Table, TableHead, TableCell, TableRow, TableBody, Pagination } from "@mui/material";
import CssBaseline from '@mui/material/CssBaseline';
import MuiDrawer from '@mui/material/Drawer';
import Box from '@mui/material/Box';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { mainListItems } from './listItems';
import axios from 'axios';
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllUser } from "../../actions/getUserAction";
import { checkRoleAction } from "../../actions/checkRoleAction";
import DeleteUserModal from "./DeleteUserModal";

function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    '& .MuiDrawer-paper': {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      boxSizing: 'border-box',
      ...(!open && {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9),
        },
      }),
    },
  }),
);

const mdTheme = createTheme();

function DashboardContent() {
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4
  };

  const [open, setOpen] = React.useState(true);
  const { users } = useSelector((reduxData) => reduxData.getUserReducer);
  const { roles } = useSelector((reduxData) => reduxData.checkRoleReducer);
  const [openModalDelete, setOpenModalDelete] = React.useState(false);
  const handleCloseDelete = () => setOpenModalDelete(false);
  const [currentUser, setCurrentUser] = React.useState("");
  const [varRefeshPage, setVarRefeshPage] =  React.useState(0);
  const dispatch = useDispatch();
  const toggleDrawer = () => {
    setOpen(!open);
  };
  const onBtnDeleteClick = (paramUser) => {
    setCurrentUser(paramUser);
    console.log(currentUser);
    setOpenModalDelete(true);
  }

  useEffect(() => {
    dispatch(checkRoleAction());
  }, []);

  useEffect(() => {
    dispatch(getAllUser());
    console.log("AAAA");
  }, [varRefeshPage]);

  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBar position="absolute" open={open}>
          <Toolbar
            sx={{
              pr: '24px', // keep right padding when drawer closed
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: '36px',
                ...(open && { display: 'none' }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              Dashboard
            </Typography>
            <IconButton color="inherit">
              <Badge badgeContent={4} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              px: [1],
            }}
          >
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          <Divider />
          <List component="nav">
            {mainListItems}
            <Divider sx={{ my: 1 }} />

          </List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: '100vh',
            overflow: 'auto',
          }}
        >
          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              <Typography component="h1" variant="h6" sx={{ textAlign: "center", width: "100%", m: 1 }}>
                List User
              </Typography>
              {
                          roles ?
              <Button className="mb-2" value="add-user" sx={{ mb: 2, borderRadius: 10, backgroundColor: "#689f38", padding: "10px 20px", fontSize: "10px" }} variant="contained">Add User +</Button>
              : ""
              }
              <TableContainer component={Paper} >     
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">User Name </TableCell>
                      <TableCell align="center">Created At</TableCell>
                      <TableCell align="center">Update at</TableCell>
                      {
                          roles ?
                         <TableCell align="center">Action</TableCell>
                      : ""
                      }
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {users.map((row, index) => (
                      <TableRow
                        key={index}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                        <TableCell align="center">{row.username}</TableCell>
                        <TableCell align="center">{row.createdAt?.slice(0, 10)}</TableCell>
                        <TableCell align="center">{row.updatedAt?.slice(0, 10)}</TableCell>
                        {
                          roles ?
                            <TableCell align="center">
                              <Button style={{ margin: "5px", borderRadius: 25, backgroundColor: "#2196f3", marginTop: "8px", padding: "10px 20px", fontSize: "10px" }} variant="contained">Sửa</Button>
                              <Button onClick={() => { onBtnDeleteClick((row)) }} style={{ margin: "5px", borderRadius: 25, backgroundColor: "#f50057", marginTop: "8px", padding: "10px 20px", fontSize: "10px" }} variant="contained">Xóa</Button>

                            </TableCell>
                            : ""
                        }

                      </TableRow>
                    ))}


                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
            <DeleteUserModal currentUser={currentUser} setVarRefeshPage ={setVarRefeshPage} openModalDelete={openModalDelete} style={style} handleCloseDelete={handleCloseDelete}/>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
}

export default function Dashboard() {
  return <DashboardContent />;
}

