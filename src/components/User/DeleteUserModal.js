import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteUserAction } from "../../actions/deleteUserAction";
import { getAllUser } from "../../actions/getUserAction";

function DeleteUserModal ({style, openModalDelete, handleCloseDelete, currentUser, varRefeshPage, setVarRefeshPage}) {
    const dispatch = useDispatch();
    const [openAlert, setOpenAlert] = useState(false)
    const [statusModalDelete, setStatusModalDelete] = useState("error")
    const [noidungAlert, setNoidungAlert] = useState("");
    const { error } = useSelector((reduxData) => reduxData.deleteUserReducer);
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnCancelClick = () => {
        handleCloseDelete()
    }
    const onBtnConfirmDeleteClick = () => {
        console.log(currentUser.username);
        dispatch(deleteUserAction(currentUser.username))
    if (error) {
      setOpenAlert(true)
      handleCloseDelete()
      setStatusModalDelete('error')
      setNoidungAlert('Xóa thất bại')
      setVarRefeshPage(varRefeshPage + 1);
    } else {
      setOpenAlert(true)
      setStatusModalDelete('success')
      handleCloseDelete()
      setNoidungAlert('Xóa thành công');
      setVarRefeshPage(varRefeshPage + 1);
      dispatch(getAllUser());
      console.log("aaaa");
    }
}
    
    return (
        <>
            <Modal
            open={openModalDelete}
            onClose={handleCloseDelete}
            aria-labelledby="modal-delete"
            aria-describedby="modal-delete-user"
            >
            <Box sx={style}>
                <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                    <strong>Delete User!</strong>
                </Typography>
                    <Row className="mt-2">
                        <Col sm="12">
                            <p>Bạn có muốn xóa không</p>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button className="bg-danger w-100 text-white" onClick={onBtnConfirmDeleteClick}>Delete</Button>
                                </Col>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={onBtnCancelClick}>Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
            </Box>
        </Modal>
        <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={statusModalDelete} sx={{ width: '100%' }}>
            {noidungAlert}
            </Alert>
      </Snackbar>
        </>
    )
}
export default DeleteUserModal
