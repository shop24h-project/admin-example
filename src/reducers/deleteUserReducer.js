import { DELETE_USER_ERROR, DELETE_USER_SUCCESS } from "../constants/deleteUser";

const initialState = {
  currentUser: {},
  error: null,
}

export default function deleteUserReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_USER_SUCCESS:
      return {
        ...state,
        currentUser: action.payload
      }
    case DELETE_USER_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
