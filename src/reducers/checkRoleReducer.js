import { CHECK_ROLE } from "../constants/checkRole";

const initialState = {
    roles: "",
}

export default function checkRoleReducer(state = initialState, action) {
    switch (action.type) {
        case CHECK_ROLE:
            return {
                ...state,
                roles: action.data
            }
        default:
            return state;
    }
}

