import { USER_REGISTER, USER_REGISTER_ERROR, USER_REGISTER_PENDING } from "../constants/userRegister";

const initialState = {
  user: {},
  pending: false,
  error: null
}

export default function userRegister(state = initialState, action) {
  switch (action.type) {
    case USER_REGISTER_PENDING:
      return {
        ...state,
        pending: true
      };
    case USER_REGISTER:
      return {
        ...state,
        user: action.payload
      }
    case USER_REGISTER_ERROR:
      return {
        ...state,
        error: action.error,
        pending: false
      };
    default:
      return state;
  }
}

