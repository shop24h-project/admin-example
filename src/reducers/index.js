import { combineReducers } from "redux";
import userRegister from "./userRegister";
import getUserReducer from "./getUserReducer";
import checkRoleReducer from "./checkRoleReducer";
import deleteUserReducer from "./deleteUserReducer";
const rootReducer = combineReducers({
    userRegister,
    getUserReducer,
    checkRoleReducer,
    deleteUserReducer
});

export default rootReducer;
