import { GET_ALL_USER } from "../constants/getUser";

const initialState = {
    users: [],
}
export default function getUserReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_USER:
            return {
                ...state,
                users: action.data
            }
        default:
            return state;
    }
}

